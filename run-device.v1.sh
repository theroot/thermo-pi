#!/usr/bin/env bash

set -o xtrace

cd "$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

export INTERVAL_SEC=600
export SENSOR=sht21
export PUBLISHER=cloudwatch

./venv/bin/python3 -u ./thermo-pi.py
