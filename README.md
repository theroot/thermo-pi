# ThermoPI

## New Development Environment

```bash
pyenv install
python3.7 -m venv ./venv
source ./venv/bin/activate
```

## Run at Startyp

Note: Update the version in `./systemd/thermo-pi.service` as applicable.

```bash
sudo cp ./systemd/thermo-pi.service /lib/systemd/system/
sudo chmod 644 /lib/systemd/system/thermo-pi.service

sudo systemctl daemon-reload
sudo systemctl enable thermo-pi.service
```

You can check in on the service with:

```bash
sudo systemctl status thermo-pi.service
```
