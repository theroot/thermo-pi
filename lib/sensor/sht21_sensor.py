from lib.sensor.sensor import SensorResult
from vendor.sht21_python.sht21 import SHT21


class SHT21Sensor(object):
    @staticmethod
    def read():
        with SHT21(1) as sht21:
            return SensorResult(
                temperature=sht21.read_temperature(),
                humidity=sht21.read_humidity()
            )
