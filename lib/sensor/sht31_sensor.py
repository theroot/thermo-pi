from lib.sensor.sensor import SensorResult
from vendor.sht21_python.sht31 import SHT31


class SHT31Sensor(object):
    @staticmethod
    def read():
        with SHT31(1) as sht31:
            temperature, humidity = sht31.get_temp_and_humidity()
            return SensorResult(
                temperature=temperature,
                humidity=humidity
            )
