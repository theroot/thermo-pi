from dataclasses import dataclass

from lib.maths.dew_point import dewpoint_approximation


@dataclass
class SensorResult:
    temperature: float
    humidity: float

    def dew_point_approximation(self):
        return dewpoint_approximation(self.temperature, self.humidity)


class Sensor(object):
    def read(self):
        # Override with implementation
        pass

