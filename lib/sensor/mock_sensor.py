from random import random

from lib.sensor.sensor import Sensor, SensorResult


class MockSensor(Sensor):
    def read(self):
        return SensorResult(
            temperature=random() * 20,
            humidity=random() * 0.5
        )
