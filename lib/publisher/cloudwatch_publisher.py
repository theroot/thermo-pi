import socket

import boto3

# Create CloudWatch client
from lib.sensor.sensor import SensorResult

cloudwatch = boto3.client('cloudwatch')

namespace = 'Langdale/ThermoPI'

dimensions = [
    {
        'Name': 'Hostname',
        'Value': socket.gethostname()
    }
]

class CloudWatchPublisher:
    def publish(self, sensor_result: SensorResult):
        print("Mock Publisher:")
        print("  Temperature: ", sensor_result.temperature)
        print("  Humidity: ", sensor_result.humidity)
        print("  Dew Point Approximation: ", sensor_result.dew_point_approximation())
        print("  Dimensions: ", dimensions)
        cloudwatch.put_metric_data(
            MetricData=[
                {
                    'MetricName': 'Temperature',
                    'Dimensions': dimensions,
                    'Unit': 'None',
                    'Value': sensor_result.temperature
                },
            ],
            Namespace=namespace
        )
        cloudwatch.put_metric_data(
            MetricData=[
                {
                    'MetricName': 'DewPoint',
                    'Dimensions': dimensions,
                    'Unit': 'None',
                    'Value': sensor_result.dew_point_approximation()
                },
            ],
            Namespace=namespace
        )
        cloudwatch.put_metric_data(
            MetricData=[
                {
                    'MetricName': 'Humidity',
                    'Dimensions': dimensions,
                    'Unit': 'Percent',
                    'Value': sensor_result.humidity
                },
            ],
            Namespace=namespace
        )
