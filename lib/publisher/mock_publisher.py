from lib.publisher.publisher import Publisher
from lib.sensor.sensor import SensorResult


class MockPublisher(Publisher):
    def publish(self, sensor_result: SensorResult):
        print("Mock Publisher:")
        print("  Temperature: ", sensor_result.temperature)
        print("  Humidity: ", sensor_result.humidity)
        print("  Dew Point Approximation: ", sensor_result.dew_point_approximation())
