from lib.sensor.sensor import SensorResult


class Publisher:
    def publish(self, sensor_result: SensorResult):
        # Implement in child
        pass
