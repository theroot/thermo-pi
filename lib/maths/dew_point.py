import sys
import numpy as np

# approximation valid for
# 0 degC < temperature < 60 degC
# 1% < relative_humidity < 100%
# 0 degC < Td < 50 degC

# constants
a = 17.271
b = 237.7  # degC


def dewpoint_approximation(temperature, relative_humidity):
    return (b * gamma(temperature, relative_humidity)) / (a - gamma(temperature, relative_humidity))


def gamma(temperature, relative_humidity):
    return (a * temperature / (b + temperature)) + np.log(relative_humidity / 100.0)
