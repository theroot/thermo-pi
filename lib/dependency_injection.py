import os
from typing import Callable

from dependency_injector import containers, providers

from lib.publisher.cloudwatch_publisher import CloudWatchPublisher
from lib.publisher.mock_publisher import MockPublisher
from lib.publisher.publisher import Publisher
from lib.scheduler.scheduler import Scheduler
from lib.sensor.mock_sensor import MockSensor
from lib.sensor.sensor import Sensor
from lib.sensor.sht21_sensor import SHT21Sensor
from lib.sensor.sht31_sensor import SHT31Sensor

interval_sec = int(os.getenv("INTERVAL_SEC", default="2"))
print("Scheduler Interval: ", interval_sec)


def select_sensor() -> Callable[[], Sensor]:
    sensor_name = os.getenv("SENSOR")
    # Oops, fixes a typo
    if sensor_name == "sht15" or sensor_name == "sht21":
        print("Using SHT21 Sensor")
        return providers.Singleton(SHT21Sensor)
    elif sensor_name == "sht31":
        print("Using SHT31 Sensor")
        return providers.Singleton(SHT31Sensor)
    else:
        print("Using Mock Sensor")
        return providers.Singleton(MockSensor)


def select_publisher() -> Callable[[], Publisher]:
    publisher_name = os.getenv("PUBLISHER")
    if publisher_name == "cloudwatch":
        print("Using CloudWatch Publisher")
        return providers.Singleton(CloudWatchPublisher)
    else:
        print("Using Mock Publisher")
        return providers.Singleton(MockPublisher)


class ApplicationContext(containers.DeclarativeContainer):
    sensor: Callable[[], Sensor] = select_sensor()

    publisher: Callable[[], Publisher] = select_publisher()

    scheduler: Callable[[], Scheduler] = providers.Singleton(
        Scheduler,
        interval_sec=interval_sec,
        sensor=sensor,
        publisher=publisher
    )
