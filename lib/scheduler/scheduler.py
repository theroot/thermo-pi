import time
from datetime import datetime

from lib.publisher.publisher import Publisher
from lib.sensor.sensor import Sensor


class Scheduler:
    def __init__(self, interval_sec: float, sensor: Sensor, publisher: Publisher):
        self.__interval_sec = interval_sec
        self.__sensor = sensor
        self.__publisher = publisher
        pass

    def start(self):
        print("Scheduler Started")
        while True:
            print("Scheduler Run: ", datetime.utcnow())
            try:
                self.__publisher.publish(self.__sensor.read())
            except Exception as ex:
                 print("An exception occurred", ex)
            print()
            time.sleep(self.__interval_sec)
