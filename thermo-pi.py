#!/usr/bin/env python3
from lib.dependency_injection import ApplicationContext

print("Hello! Starting Thermo-Pi")
ApplicationContext.scheduler().start()
